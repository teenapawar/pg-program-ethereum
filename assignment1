// SPDX-License-Identifier: MIT
//Name = Teena pawar
//Assignment = 1 ( Voter Application)

pragma solidity ^0.7.0;

contract Voting{

    //Owner of the contract, address which deploys the contract
    address owner;  

    // constructor is initialized when smartcontract is deployed
    // and it has owner as the address which creates the contract
    constructor () {
        owner = msg.sender;
    }

    // to store the voter info
    struct Voter {
        uint candidateIDVote;
        bool hasVoted;
         bool isAuthorized;
    }

    // to store the candidate info
    struct Candidate {
        string name;
        string party;
        uint noOFVotes;
        bool doesExist;

    }

    //Contract level variables to capture the state of the contract
    uint numCandidates;
    uint numVoters;
    uint numOfVotes;

    // mapping for the voters & candidates
    mapping (uint => Candidate) candidates;
    mapping ( address => Voter) voters;

    //OnlyOwner of the contract can add candidates
    function addCandidate (string memory name, string memory party) public{
        uint candidateID = numCandidates++;
        candidates[candidateID] = Candidate ( name,party,0,true);
    }

    // To get info about candidate
    function getCandidate (uint candidateID) public view returns (string memory , string memory,uint, bool){
        return (candidates[candidateID].name ,candidates[candidateID].party ,candidateID,candidates[candidateID].doesExist);
    }

    //  Vote for candidate
    //Anyone can vote as long as they are authorized & have not voted earlier
    function Vote ( uint candidateID) public {
        require ( !voters[msg.sender].hasVoted);
        require (voters[msg.sender].isAuthorized); 
        if (candidates[candidateID].doesExist == true) {
            voters[msg.sender] = Voter(candidateID, true, true);
            candidates[candidateID].noOFVotes ++;
            numOfVotes++;
            numVoters++;
        }
        
    }

     modifier onlyOwner() {
        require(msg.sender == owner);
    _; //This step will ensure that next function call happens
    }

    
    //Only owner of the contract can authorize other voters
     function authorize(address voterAdd) public onlyOwner {
        voters[voterAdd].isAuthorized = true;
    }

    // to get total votes of perticular candidate
    function totalVotesOfCandidate(uint candidateID) view public returns (uint) {
        return candidates [candidateID].noOFVotes;
    }

    // to get  no of candidates
    function getNumOfCandidates() public view returns (uint) {
        return numCandidates;
    }

    // to get no of voters
    function getNumOfVoters() public view returns (uint) {
        return numVoters;
    }

    //Only owner of the contract can authorize other voters
    // this func returns the total votes of overall voting 
     function totalVotesOfVoting() public onlyOwner view returns (uint) {
        return numOfVotes;
    }

  
}
